# Globo challenge
This project aims to implement the challenges proposed in the Globo selection process.

Two challenges were proposed and both were solved completely.

Technologies used to implement the project were:

- Java, JPA, EJB, PostgreSQL, Maven, JUnit, Wildfly Server, Apache Server, JAX-RS, Git, AWS EC2, AWS Router 53, JWT

## Challenge 1 - Script

To run challenge 1, run the command at the root of the project: 

`mvn exec:java -Dexec.mainClass="br.com.globo.challenge.standalone.Challenge1"`

After running follow the steps:

1) Type the directory of your file: (Ex: C:\\Users\\jpss2\\Desktop\\spreadsheet.txt)

2) Type the directory of your output file: (Ex: C:\\Users\\jpss2\\Desktop\\output spreadsheet.txt)

Your answer file will be in the directory you filled in

NOTE: To run the unit tests run the following commands at project root
`mvn install`
`mvn test`

NOTE: The challenge example file is in the `br.com.globo.challenge.standalone package`

## Challenge 2 - Project WEB

### [Demo](http://www.desafio-globo.jpss.com.br/) 

Click on the Demo link to access the application in production

To log into the system, enter the following credentials:
username: `admin`
passowrd: `admin`

## Api Documentation

I used the swagger to expose the implemented API and you can access it from this [link](http://www.desafio-globo.jpss.com.br/backend-globo-challenge-api/index.html) 




