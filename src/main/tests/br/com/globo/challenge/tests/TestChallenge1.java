package br.com.globo.challenge.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.Map;
import java.util.Vector;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.com.globo.challenge.standalone.Challenge1;

class TestChallenge1 {

	@DisplayName("Success Convert Byte to Giga and result >=1")	
	@Test
	void byteToGigaBetterOrEqualThen1() {
		assertEquals(Challenge1.formatByte("24696061952", Challenge1.G, "GB"), "23.00GB");
	}
	
	@DisplayName("Success Convert Byte to Giga and result >=1")	
	@Test
	void byteToGigaLessThen1() {
		assertEquals(Challenge1.formatByte("657019718", Challenge1.G, "GB"), "0,61GB");
	}
	
	@DisplayName("Build movie")	
	@Test
	void buildMovie() {
		Vector<String> columns = new Vector<String>();
		columns.add("name");
		columns.add("date");
		columns.add("path");
		columns.add("create_by");
		columns.add("size");
		
		String row = "El Hoyo                  21-02-2020  /Mount/movies/ElHoyo.mp4                Galder Gaztelu-Urrutia      657019718";
		Map<String, Object> movie = Challenge1.buildMovie(columns, row);
		String result = "{date=21/02/2020, path=/Mount/movies/ElHoyo.mp4, size=0,61GB, name=El Hoyo}";
		
		assertEquals(movie.toString(), result);
	}
	
	@DisplayName("Build array of movies")	
	@Test
	void mountMovies() {
		String path = "src/main/tests/br/com/globo/challenge/tests/planilha.txt";
		File file = new File(path);
		String movies = Challenge1.mountStrJson(file);
		String result = "[{\"date\":\"10/03/2019\",\"path\":\"/Mount/movies/Joker.mxf\",\"size\":\"23GB\",\"name\":\"Joker\"},{\"date\":\"07/11/2019\",\"path\":\"/Mount/movies/Parasite.mxf\",\"size\":\"33GB\",\"name\":\"Parasite\"},{\"date\":\"27/11/2019\",\"path\":\"/Mount/movies/Frozen.mxf\",\"size\":\"46GB\",\"name\":\"Frozen\"},{\"date\":\"19/12/2019\",\"path\":\"/Mount/movies/Star_wars.mxf\",\"size\":\"29GB\",\"name\":\"Star Wars\"},{\"date\":\"25/04/2019\",\"path\":\"/Mount/movies/EndGame.mxf\",\"size\":\"130GB\",\"name\":\"Avengers: EndGame\"},{\"date\":\"11/10/2019\",\"path\":\"/Mount/movies/Fractured.mp4\",\"size\":\"0,72GB\",\"name\":\"Fractured\"},{\"date\":\"18/01/2020\",\"path\":\"/Mount/movies/1917.mxf\",\"size\":\"59GB\",\"name\":\"1917\"},{\"date\":\"28/03/2019\",\"path\":\"/Mount/movies/Dumbo.mxf\",\"size\":\"71GB\",\"name\":\"Dumbo\"},{\"date\":\"26/09/2019\",\"path\":\"/Mount/movies/AdAstra.mxf\",\"size\":\"89GB\",\"name\":\"Ad Astra\"},{\"date\":\"17/01/2019\",\"path\":\"/Mount/movies/Glass.mxf\",\"size\":\"43GB\",\"name\":\"Glass\"},{\"date\":\"21/02/2020\",\"path\":\"/Mount/movies/ElHoyo.mp4\",\"size\":\"0,61GB\",\"name\":\"El Hoyo\"},{\"date\":\"01/01/1988\",\"path\":\"/Mount/movies/NuovoCinemaParadiso.mkv\",\"size\":\"1GB\",\"name\":\"Nuovo Cinema Paradiso\"},{\"date\":\"04/04/2019\",\"path\":\"/Mount/movies/Shazam.mxf\",\"size\":\"63GB\",\"name\":\"Shazam\"},{\"date\":\"21/03/2019\",\"path\":\"/Mount/movies/Us.mxf\",\"size\":\"67GB\",\"name\":\"Us\"},{\"date\":\"07/02/2019\",\"path\":\"/Mount/movies/Lego_Movie.mxf\",\"size\":\"87GB\",\"name\":\"Lego Movie\"}]";
		
		assertEquals(movies, result);
	}
}
