package br.com.globo.challenge.standalone;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;

import com.google.gson.Gson;

//Crie um programa que leia as colunas do txt "planilha.txt" (em anexo) e crie com as 
//infos coletadas um json com apenas os seguintes atributos: 
//name, path, date, size. 
//Requisitos: 
//- O json deve ser escrito em um arquivo. 
//- O campo date deve estar no padr�o brasileiro (dd/mm/yyyy). 
//- O campo size deve ser humanizado utilizando GB como multiplicador e ter no m�ximo duas 
//casas decimais, caso o arquivo seja menor que 1 GB. Utilize v�rgula como separador de parte 
//decimal.

// mvn exec:java -Dexec.mainClass="br.com.globo.challenge.standalone.Challenge1"  

public class Challenge1 {
	
	public static SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy"); 
	public static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
	public static final long K = 1024;
	public static final long M = K * K;
	public static final long G = M * K;	
	
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("Type the directory of your file: (Ex: C:\\Users\\jpss2\\Desktop\\planilha.txt)");		
		String directoryFile = in.nextLine();
		
		System.out.println("");
		System.out.println("");
		System.out.println("Type the directory of your output file: (Ex: C:\\Users\\jpss2\\Desktop\\output planilha.txt)");		
		String outputDirectoryFile = in.nextLine();
		
		File file = new File(directoryFile);
		
		String json = Challenge1.mountStrJson(file);		
		writeInFile(json, outputDirectoryFile);
	}
	
	public static String mountStrJson(File file) {
		try {
			Scanner in = new Scanner(file);
			int i = 1;
			Vector<String> columns = new Vector<String>();
			Gson gson = new Gson();
			Vector<Map<String, Object>> movies = new Vector<>();

			while (in.hasNextLine()) {
				
				String line = in.nextLine();

				if (line != null && !line.trim().isEmpty()) {
					if (i == 1)
						columns = getColumns(line);
					else {
						Map<String, Object> movie = buildMovie(columns, line);
						movies.add(movie);						
					}
					i++;
				}
			}

			in.close();
			return gson.toJson(movies);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Vector<String> getColumns(String row) {

		Vector<String> columns = new Vector<String>();

		for (String column : row.split(" ")) {
			if (!column.isEmpty()) {
				columns.add(column);
			}
		}

		return columns;
	}

	public static Map<String, Object> buildMovie(Vector<String> columns, String row) {
		
		Map<String, Object> movie = new HashMap<>();
		Vector<String> registers = new Vector<String>();
		
		for(String register: row.split(" ")) {			
			if(!register.isEmpty()) {
				registers.add(register);
			}			
		}
		
		String size = Challenge1.formatByte(registers.get(registers.size() - 1), G, "GB");		
		movie.put(columns.get(columns.size() - 1), size);
		
		int indexColumn = 0;
		String name = "";
		
		for(String register: registers) {
			
			Date date = getDate(register);
			
			if(date != null) {
				indexColumn++;				
				movie.put(columns.get(indexColumn), dateFormat.format(date));
				indexColumn++;
				continue;
			}
			
			if(indexColumn == 0) {
				name += register + " ";
				register =  name.substring(0, name.length() - 1);
				movie.put(columns.get(0), register);
			}
			
			if(indexColumn > 0) {
				movie.put(columns.get(indexColumn), register);
				break;
			}
		}
		
		return movie;
	}
	
	public static String formatByte(String valueStr, long divider,  String unit){
		long value = Long.parseLong(valueStr);
		double result =  divider > 1 ? (double) value / (double) divider : (double) value;
		String resultStr = result < 1 ? String.format("%.2f", result) : String.format("%.0f", result);
		
		return resultStr + unit;
	}
	
	public static Date getDate(String value) {		
		try {
			return format.parse(value);
		}
		catch(Exception e) {
			return null;
		}
	}
	
	private static void writeInFile(String movie, String directory) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(directory));
		    writer.write(movie);
		    
		    System.out.println("");
			System.out.println("");
		    System.out.println("Success to create your File");
		    System.out.println("");
			System.out.println("");
		    writer.close();
		}
		catch(Exception e) {
			System.out.println("CAN'T WRITE FILE");
		}
		
	}
}
