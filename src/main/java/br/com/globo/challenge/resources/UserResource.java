package br.com.globo.challenge.resources;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.globo.challenge.controller.UserController;
import br.com.globo.challenge.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(produces= "application/json")
@Path("/user")
@Produces({ MediaType.APPLICATION_JSON })
public class UserResource {
	
	@EJB
	private UserController userController;
	
	@GET
	@Path("/{id}")
	@ApiOperation(value = "Recover user", 
		    notes = "",
		    response = User.class)
	public Response getUser(@PathParam("id") Integer userID) {
		try {
			User user = this.userController.find(userID);
			return Response.ok(user).build();
		}
		catch(Exception e) {
			return Response.ok().status(Status.NOT_FOUND).build();
		}
	}
	
}
