package br.com.globo.challenge.resources;

import java.nio.file.AccessDeniedException;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.globo.challenge.controller.AuthController;
import br.com.globo.challenge.controller.TokenController;
import br.com.globo.challenge.controller.UserController;
import br.com.globo.challenge.model.User;
import br.com.globo.challenge.security.JWTUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(produces= "application/json")
@Path("/auth")
@Produces({ MediaType.APPLICATION_JSON })
public class AuthResource {
	
	@EJB
	private UserController userController;
	
	@EJB
	private TokenController tokenController;
	
	@EJB
	private AuthController authController;
	
	@POST
	@Path("/logout")
	public Response logout() {
		this.authController.logout();
		return Response.ok().entity("Logout success").build();
	}

	
	@POST
	@Path("/")
	@ApiOperation(value = "Authentication in application", 
		    notes = "",
		    response = String.class)
	public Response authenticate(@QueryParam("username") String username,
					    @QueryParam("password") String password) {
		try {
			this.authController.login(username, password);
			return Response.ok(this.authController.getToken()).build();
		}
		catch (Exception e) {
			return Response.ok(this.authController.getToken()).status(Status.UNAUTHORIZED).build(); 
		}
	}
}
