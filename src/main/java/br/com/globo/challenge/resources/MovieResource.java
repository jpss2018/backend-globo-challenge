package br.com.globo.challenge.resources;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.globo.challenge.controller.FacadeMovieController;
import br.com.globo.challenge.model.MovieDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

//Precisamos de uma aplica��o web onde o usu�rio possa escolher filtrar os resultados de uma
//base de dados fornecida pela API REST do Ghibli Studios selecionando o nome do diretor ou
//ano do lan�amento ou por palavras no nome ou descri��o do filme. Diversos filtros podem ser
//utilizados ao mesmo tempo em conjunto.

//DICA: Utilize os endpoints https://ghibliapi.herokuapp.com/films e
//https://ghibliapi.herokuapp.com/people como fonte de dados. Para mais informa��es,
//consulte a documenta��o em https://ghibliapi.herokuapp.com .

//Ap�s esse filtro o usu�rio visualiza o t�tulo, a descri��o, o diretor, os nomes dos personagens
//dos filmes e o score dos filmes numa tabela.

//Esta aplica��o deve ser divida em backend e frontend. A aplica��o de frontend deve consultar
//uma API do backend e n�o diretamente a API do Ghibli.
//N�o definimos um layout, logo, fica a crit�rio e criatividade do desenvolvedor. Sinta-se livre
//para inovar! 

@Api(produces= "application/json")
@Path("/movie")
@Produces({ MediaType.APPLICATION_JSON })
public class MovieResource {
	
	@EJB
	private FacadeMovieController facadeMovieController; 
	
	@GET
	@Path("/")
	@ApiOperation(value = "Get movies", 
		    notes = "Multiple query values can be provided to recover the movie list",
		    response = MovieDTO.class,
		    responseContainer = "List")
	public Response get(
			 			@QueryParam("title") String title,
					    @QueryParam("director") String director,
					    @QueryParam("releaseDate") String releaseDate, 
					    @QueryParam("description") String description) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		if(title != null) params.put("title", title);
		if(director != null) params.put("director", director);
		if(description != null) params.put("description", description);
		if(releaseDate != null) params.put("release_date", releaseDate);
		
		List<MovieDTO> movies = facadeMovieController.getMovies(params);
		
		return Response.ok(movies).build();
	}
	
}
