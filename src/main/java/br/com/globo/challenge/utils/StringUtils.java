package br.com.globo.challenge.utils;

public class StringUtils {
	
	/**
	 * 
	 * @param str
	 * @return It is true if str == null or str is empty 
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.isEmpty();
	}

}
