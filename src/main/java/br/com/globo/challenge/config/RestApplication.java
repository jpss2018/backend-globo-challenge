package br.com.globo.challenge.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import io.swagger.jaxrs.config.BeanConfig;

@ApplicationPath("/rest")
public class RestApplication extends Application{

	 public RestApplication() {
	    BeanConfig conf = new BeanConfig();
	    conf.setTitle("Globo Challenge API");
	    conf.setDescription("Resources ");
	    conf.setVersion("1.0.0");
	    conf.setHost("localhost:8080");
	    conf.setBasePath("/backend-globo-challenge-api/rest");
	    conf.setSchemes(new String[] { "http" });
	    conf.setResourcePackage("br.com.globo.challenge.resources");
	    conf.setScan(true);
	  }
}
