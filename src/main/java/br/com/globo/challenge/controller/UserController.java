package br.com.globo.challenge.controller;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.globo.challenge.model.User;

@Stateless
public class UserController {
	
	@PersistenceContext
	private EntityManager manager;
	
	public User get(String username) {		
		Query query = this.manager.createQuery(" FROM " + User.class.getName() + " WHERE username = :username");
		query.setParameter("username", username);
		
		try {
			return (User) query.getSingleResult();
		}
		catch (Exception e) {
			return null;
		}
	}
	
	public User find(Integer id) {		
		return this.manager.find(User.class, id);
	}
}
