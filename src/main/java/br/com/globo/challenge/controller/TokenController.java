package br.com.globo.challenge.controller;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.globo.challenge.model.Token;
import br.com.globo.challenge.model.User;
import br.com.globo.challenge.security.JWTUtils;
import io.jsonwebtoken.Claims;

@Stateless
public class TokenController {

	@PersistenceContext
	private EntityManager manager;
	
	@EJB
	private UserController userController;
	
	public Token create(String jwtToken) {
		
		Token token = new Token();
		token.setToken(jwtToken);
		token.setStatus("active");
		
		Claims claims = JWTUtils.getClaims(jwtToken);
		
		if(claims.containsKey("sub")) {
			User user = userController.get(claims.getSubject());
			
			if(user == null) throw new NullPointerException("User not found");
			
			token.setUser(user);
		}
		
		if(claims.containsKey("exp")) 		  token.setExpireIn(claims.getExpiration());
		if(claims.getIssuedAt() != null) 	  token.setCreateIn(claims.getIssuedAt());
		
		manager.persist(token);
		
		return token; 
	}
	
	public void inactiveToken(Integer tokenId) {

		Token token = manager.find(Token.class, tokenId);
		token.setStatus("inactive");
		token.setInactiveIn(new Date());

		manager.merge(token);
	}
}
