package br.com.globo.challenge.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.globo.challenge.model.Movie;
import br.com.globo.challenge.utils.StringUtils;

@Stateless
public class MovieController {

	public List<Movie> getMovies(Map<String, Object> params) {
		String response = ConnectionHerokuapp.get("/films", params);
		Type collectionType = new TypeToken<ArrayList<Movie>>() {
		}.getType();

		List<Movie> movies = new Gson().fromJson(response, collectionType);
		movies = movies.stream().filter(movie -> hasFilter(params, movie)).collect(Collectors.toList());

		return movies;
	}
	
	private boolean hasFilter(Map<String, Object> params, Movie movie) {
		String paramTitle = params.get("title") != null ? (String) params.get("title") : null;
		String paramDirector = params.get("director") != null ? (String) params.get("director") : null;
		String paramReleaseDate = params.get("release_date") != null ? (String) params.get("release_date") : null;
		String paramDescription = params.get("description") != null ? (String) params.get("description") : null;
		
		boolean ruleA = StringUtils.isEmpty(paramTitle) ? true : movie.getTitle() != null && movie.getTitle().toLowerCase().contains(paramTitle.toLowerCase()); 
		boolean ruleB = StringUtils.isEmpty(paramDirector) ? true : movie.getDirector() != null && movie.getDirector().toLowerCase().contains(paramDirector.toLowerCase());
		boolean ruleC = StringUtils.isEmpty(paramReleaseDate) ? true :  movie.getRelease_date() != null && movie.getRelease_date().toLowerCase().contains(paramReleaseDate.toLowerCase());
		boolean ruleD = StringUtils.isEmpty(paramDescription) ? true : movie.getDescription() != null && movie.getDescription().toLowerCase().contains(paramDescription.toLowerCase()); 
		
		return ruleA && ruleB && ruleC && ruleD;
	}
	
}
