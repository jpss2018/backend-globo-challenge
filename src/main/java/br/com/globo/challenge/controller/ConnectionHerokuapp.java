package br.com.globo.challenge.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

class ConnectionHerokuapp {

	static final String url = "https://ghibliapi.herokuapp.com";
	
	public static String get(String path, Map<String, Object> params){
		
		if(path == null) path = "";
		
        try{
        	HttpGet request = new HttpGet(ConnectionHerokuapp.url + path);
    		URI uri = ConnectionHerokuapp.buildUri(path, params);
    		
    		// Parameters only work if you want to be exactly the same 
//    		if(uri != null) request = new HttpGet(ConnectionHerokuapp.url + uri);
        	
        	CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(request);

            HttpEntity entity = response.getEntity();
            return entity != null ? EntityUtils.toString(entity) : null;

        }      
        catch (IOException e) {
        	System.out.println("Can't recover data");
			e.printStackTrace();
			return null;
		}
	}
	
	private static URI buildUri(String url, Map<String, Object> params) {
		
		if(params == null) return null;
		
		try {
			URIBuilder uriBuilder = new URIBuilder(url);
			
			for(String key: params.keySet()) {
				
				Object value = (Object) params.get(key);
				
				if(value != null) {
					if(value.getClass().isArray() || value instanceof Collection) {
						for(Object obj: (Collection<?>)value) {
							if(obj instanceof String) uriBuilder.addParameter(key, (String) obj);
						}
					}
					else if(value instanceof String){
						String valueStr = (String) value;
						
						if(!valueStr.isEmpty()) uriBuilder.addParameter(key, valueStr);
					}
					else if(value instanceof Long){
						uriBuilder.addParameter(key, value.toString());
					}
				}
			}
			
			return uriBuilder.build();
		}
	    catch(URISyntaxException e) {
        	System.out.println("Can't build uri");
        	e.printStackTrace();
        	return null;
        }
	}
}
