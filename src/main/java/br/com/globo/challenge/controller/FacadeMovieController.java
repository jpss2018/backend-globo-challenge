package br.com.globo.challenge.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.globo.challenge.model.Movie;
import br.com.globo.challenge.model.MovieDTO;
import br.com.globo.challenge.model.Person;

@Stateless
public class FacadeMovieController {
	
	@EJB
	private MovieController movieController;
	
	@EJB
	private PeopleController peopleController;
	
	final String urlPeople = "https://ghibliapi.herokuapp.com/people/";
	
	public List<MovieDTO> getMovies(Map<String, Object> params){
		
		List<Movie> movies = this.movieController.getMovies(params);
		Map<String, Object> peopleParam = new HashMap<>();
		Set<String> peopleIds = new HashSet<String>();
		
		peopleParam.put("id", peopleIds);
		List<Person> people = this.peopleController.getPeople(null);
		List<MovieDTO> moviesDTO = new ArrayList<>();
		
				
		for(Movie movie: movies) {
			MovieDTO movieDto = new MovieDTO(movie);
			Set<Person> personList = new HashSet<Person>();
			
			for(String id: movieDto.getPeople()) {
				
				if(id.equals(urlPeople)) {
					personList.addAll(people);
				}
				else{
					for(Person person: people) {
						if(id.replaceAll(urlPeople, "").equals(person.getId())) {
							personList.add(person);
						}
					}	
				}
			}
			
			if(personList != null) movieDto.setPerson(new ArrayList(personList));
			
			moviesDTO.add(movieDto);
		}
		
		return moviesDTO;
	}
	
	
}
