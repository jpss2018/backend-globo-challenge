package br.com.globo.challenge.controller;

import java.nio.file.AccessDeniedException;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.mindrot.jbcrypt.BCrypt;

import br.com.globo.challenge.model.Token;
import br.com.globo.challenge.model.User;
import br.com.globo.challenge.security.JWTUtils;

@Singleton
public class AuthController {
	
	@PersistenceContext
	private EntityManager manager;
	
	@EJB
	private UserController userController;
	
	@EJB
	private TokenController tokenController;
	
	private User loggedUser;
	
	private Token token;
	
	public void login(String username, String password) throws AccessDeniedException {
		Query query = this.manager.createQuery(" FROM " + User.class.getName() + " WHERE username = :username");
		query.setParameter("username", username);
		
			User user = (User) query.getSingleResult();
			
			if(BCrypt.checkpw(password, user.getPassword())) {	
				String jwtToken = JWTUtils.createJWT(user.getId().toString(), "issuer", user.getUsername(), 60000*60*24, "audience");
				this.loggedUser = user;
				this.token = this.tokenController.create(jwtToken);
			}
			else {
				this.loggedUser = null;
				this.token = null;
				throw new AccessDeniedException("PASSWORD WRONG");
			}			
	}
	
	public void logout() {
		if(this.loggedUser != null) {
			this.tokenController.inactiveToken(this.loggedUser.getId());
			this.loggedUser = null;
		}
	}

	public User getLoggedUser() {
		return loggedUser;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}	
	
	
}
