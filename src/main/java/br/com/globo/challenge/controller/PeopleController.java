package br.com.globo.challenge.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.globo.challenge.model.Person;

@Stateless
public class PeopleController {
	public List<Person> getPeople(Map<String, Object> params) {
		String response = ConnectionHerokuapp.get("/people", params);
		Type collectionType = new TypeToken<ArrayList<Person>>(){}.getType();
		return new Gson().fromJson(response, collectionType);
	}
}
