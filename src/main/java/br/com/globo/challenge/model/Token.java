package br.com.globo.challenge.model;

import java.io.Serializable;
import java.util.Date;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="token")
public class Token  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
		
	@NotNull
	@Column(name="expire_in")
	private Date expireIn;
	
	@Column(name="create_in")
	@NotNull
	@JsonbDateFormat(value="dd/MM/yyyy HH:mm", locale="pt-BR")
	private Date createIn;
	
	@Column(name="inactive_in")
	@JsonbDateFormat(value="dd/MM/yyyy HH:mm", locale="pt-BR")
	private Date inactiveIn;
	
	@NotNull
	private String token;
	
	@Column(name="token_refresh")
	private String tokenRefresh;
	
	private String status;
	
	@ManyToOne
	@NotNull
	@JoinColumn(name="subject_id")
	private User user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(Date expireIn) {
		this.expireIn = expireIn;
	}

	public Date getCreateIn() {
		return createIn;
	}

	public void setCreateIn(Date createIn) {
		this.createIn = createIn;
	}

	public Date getInactiveIn() {
		return inactiveIn;
	}

	public void setInactiveIn(Date inactiveIn) {
		this.inactiveIn = inactiveIn;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTokenRefresh() {
		return tokenRefresh;
	}

	public void setTokenRefresh(String tokenRefresh) {
		this.tokenRefresh = tokenRefresh;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
