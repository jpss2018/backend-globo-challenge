package br.com.globo.challenge.model;

import java.util.List;

public class Movie {
	
	private String id, title, description, director, release_date;	
	
	private Long rt_score;	
	
	private List<String> people;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Long getRt_score() {
		return rt_score;
	}

	public void setRt_score(Long rt_score) {
		this.rt_score = rt_score;
	}

	public List<String> getPeople() {
		return people;
	}

	public void setPeople(List<String> people) {
		this.people = people;
	}

	public String getRelease_date() {
		return release_date;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}
}
