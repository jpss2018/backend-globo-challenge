package br.com.globo.challenge.model;

import java.util.List;

public class MovieDTO extends Movie {
	
	private List<Person> person;
	
	public MovieDTO(Movie movie) {
		this.setId(movie.getId());
		this.setTitle(movie.getTitle());
		this.setPeople(movie.getPeople());
		this.setDirector(movie.getDirector());
		this.setRt_score(movie.getRt_score());
		this.setDescription(movie.getDescription());
		this.setRelease_date(movie.getRelease_date());
	}

	public List<Person> getPerson() {
		return person;
	}

	public void setPerson(List<Person> person) {
		this.person = person;
	}	
}
