package br.com.globo.challenge.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;

import com.google.gson.JsonObject;

import io.jsonwebtoken.Claims;

@WebFilter(urlPatterns = {"/rest/*"}, description = "JWT Filter")
public class JWTFilter implements Filter{
	
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
	    HttpServletRequest req = (HttpServletRequest) servletRequest;
	    HttpServletResponse res = (HttpServletResponse) servletResponse;
	    
	    if(req.getMethod().equalsIgnoreCase("OPTIONS")){
	        filterChain.doFilter(servletRequest, servletResponse);
	        return;
	    }
	    
	    if(req.getRequestURI().contains("/rest/auth") || req.getRequestURI().contains("rest/swagger.json")) {
	    	filterChain.doFilter(servletRequest, servletResponse);
	    	return; 
	    }
	    
	    String token = req.getHeader(JWTUtils.HEADER_STRING);

	    if(token == null || token.trim().isEmpty()){
	        res.setStatus(401);
	        return;
	    }

	    try {
	    	Claims claims = JWTUtils.getClaims(token);
	        filterChain.doFilter(servletRequest, servletResponse);
	    	
	    } catch (Exception e) {
	        res.setStatus(401);
	        
	        JsonObject message = new JsonObject();
	        message.addProperty("msg", e.getMessage());
    		
    		PrintWriter out = res.getWriter();
    		res.setContentType("application/json");
    		res.setCharacterEncoding("UTF-8");
    		res.addHeader("Access-Control-Allow-Origin", "*");
    		out.print(message);
    		out.flush();    		
	    }
	}

	@Override
	public void destroy() {	    
	}
}

