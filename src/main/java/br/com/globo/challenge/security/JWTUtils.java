package br.com.globo.challenge.security;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTUtils {

	public static final String SECRET = "GLOBO_CHALLENGE_SECRET";
	public static final String TOKEN_PREFIX = "Bearer";
	public static final String HEADER_STRING = "Authorization";
	public static final String ISSUER = "jpss";
	
	
	/**
	 * 
	 * @param id
	 * @param issuer
	 * @param subject
	 * @param ttlMillis
	 * @param audience
	 * @return
	 */
	public static String createJWT(String id, String issuer, String subject, long ttlMillis, String audience) {
		 
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS512;
	 
	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);
	    Date expiration = null;
	    
	    if (ttlMillis >= 0) {
	    long expMillis = nowMillis + ttlMillis;
	        expiration = new Date(expMillis);
	    }
	 
	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET);
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
	
	    JwtBuilder builder = Jwts.builder()
	    						.setId(id)
	    						.setIssuer(ISSUER)
	    						.setIssuedAt(now)
	    						.setSubject(subject)
                                .setAudience(audience)
                                .setExpiration(expiration)
                                .claim("type", TOKEN_PREFIX)
                                .signWith(signatureAlgorithm, signingKey);
	    
	 
	    return builder.compact();
	}
	
	public static Claims getClaims(String token) {
		 
		Claims claims = Jwts.parser()
					.setSigningKey(DatatypeConverter.parseBase64Binary(SECRET))
					.parseClaimsJws(token.replace(JWTUtils.TOKEN_PREFIX, ""))
					.getBody();
		
		return claims;
	}
}

